package pventana;

import junit.framework.TestCase;

public class TestPrincipalventana extends TestCase {

    // Metodo suma
    public void testSuma() {
        assertEquals(10, Operaciones.suma(5));
    }

    // Metodo resta
    public void testResta() {
        assertEquals(0, Operaciones.resta(5));
    }

    // Metodo multiplicacion
    public void testMult() {
        assertEquals(25, Operaciones.mult(5));
    }

    // Metodo div
    public void testDiv() {
        assertEquals(1, Operaciones.div(5));
    }

}