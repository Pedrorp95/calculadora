package pventana;

//Clase para las operaciones
public class Operaciones {

  int n;

  //Metodo suma
  public static int suma(int n) {
      int suma=n+n;
      return suma;
  }

  //Metodo resta
  public static int resta(int n) {
      int resta=n-n;
      return resta;
  }

  //Metodo multiplicacion
  public static int mult(int n) {
      int mult=n*n;
      return mult;
  }

  //Metodo division
  public static int div(int n) {
      int div;
      //Hacemo un IF por si es division entre 0
      if(n!=0) {
          div=n/n;
      }else {
          System.out.println("No se puede dividir por 0");
          div=0;
      }
      return div;
  }


}