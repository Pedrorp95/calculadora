package pventana;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Ventana extends JFrame{
    //Creamos el constructor (mismo nombre que la clase)
    public Ventana(){
    	//Establecer titulo a la ventana.
        setTitle("Calculadora Pedro");
    	//Establecemos el tama�o de la ventana.
    	this.setSize(500, 600);
    	//Centralizar la ventana predeterminadamente al centro
    	setLocationRelativeTo(null);
        //metodo para finalizar la ventana se cierre
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        //Llama al metodo.
        IniciarComponentes();
    }
    
    private void IniciarComponentes() {
    	//Creaci�n de un panel.
    	JPanel panel = new JPanel();
    	panel.setLayout(null); //dise�o off
    	//Agregar el panel a la ventana y color.
    	this.getContentPane().add(panel);
    	panel.setBackground(Color.white);

    	//Creamos una etiqueta
    	JLabel etiqueta = new JLabel();
    	etiqueta.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(etiqueta);
        
    	
    	//Creamos botones
    	JButton boton0 = new JButton();
    	JButton boton1 = new JButton();
    	JButton boton2 = new JButton();
    	JButton boton3 = new JButton();
    	JButton boton4 = new JButton();
    	JButton boton5 = new JButton();
    	JButton boton6 = new JButton();
    	JButton boton7 = new JButton();
    	JButton boton8 = new JButton();
    	JButton boton9 = new JButton();
    	JButton boton10 = new JButton();
    	JButton boton11= new JButton();
    	JButton boton12= new JButton();
    	JButton boton13= new JButton();
    	
    	//Posicion de los botones, coord x,y,ancho,largo
    	boton0.setBounds(2, 300, 120, 70);
    	boton1.setBounds(120, 300, 120, 70);
    	boton2.setBounds(240,300,120,70);
    	boton3.setBounds(2,370,120,70);
    	boton4.setBounds(120,370,120,70);
    	boton5.setBounds(240,370,120,70);
    	boton6.setBounds(2,440,120,70);
    	boton7.setBounds(120,440,120,70);
    	boton8.setBounds(240,440,120,70);
    	boton9.setBounds(360,440,120,70);
    	boton10.setBounds(360,370,120,70);
    	boton11.setBounds(360,300,120,70); 
    	boton12.setBounds(360,510,120,40);
    	boton13.setBounds(2,510,358,40);
    	
    	
    	//Establecemos texto al boton
    	boton0.setText("7");
    	boton1.setText("8");
    	boton2.setText("9");
    	boton3.setText("4");
    	boton4.setText("5");
    	boton5.setText("6");
    	boton6.setText("1");
    	boton7.setText("2");
    	boton8.setText("3");
    	boton9.setText("+");
    	boton10.setText("-");
    	boton11.setText("x");
    	boton12.setText("=");
    	boton13.setText("0");
    	
    	//Alt + caracter, en este caso no s� como se establece sin el alt
    	//Para que al pulsar alt + caracter se pulse el boton de la calculadora.
    	boton0.setMnemonic('7');
    	boton1.setMnemonic('8');
    	boton2.setMnemonic('9');
    	boton3.setMnemonic('4');
    	boton4.setMnemonic('5');
    	boton5.setMnemonic('6');
    	boton6.setMnemonic('1');
    	boton7.setMnemonic('2');
    	boton8.setMnemonic('3');
    	boton13.setMnemonic('0');
    	
    	//Agregamos los botones al panel.
    	panel.add(boton0);
    	panel.add(boton1);
    	panel.add(boton2);
    	panel.add(boton3);
    	panel.add(boton4);
    	panel.add(boton5);
    	panel.add(boton6);
    	panel.add(boton7);
    	panel.add(boton8);
    	panel.add(boton9);
    	panel.add(boton10);
    	panel.add(boton11);
    	panel.add(boton12);
    	panel.add(boton13);
    	
    	//Colocar caja de texto
    	JTextField Resultado = new JTextField("Resultado",20);
    	Resultado.setBounds(16, 16, 448, 100);
    	//Resultado.setText("El resultado es...");
    	panel.add(Resultado);
    	
    }
}